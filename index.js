const express = require('express');
const app = express();
const PORT = 3000;
const mongoose = require('mongoose');


//middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://amielvb:amielvb12@batch139.ye6o6.mongodb.net/todo_tasks?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true});

//get notification if successfully connected or not
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log(`Connected to database`));

let userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model("Task", userSchema)

app.post("/signup", (req, res) => {
	User.findOne({ username: req.body.username }, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send(`Username already exists!`);
		} else {
			let newUser = new User({username: req.body.username});

			newUser.save((err, savedUser) => {
				if (err){
					return console.error(err);
				} else {
					return res.send(`User Created!`)
				}
			})
		}
	})
})


app.listen(PORT, () => console.log(`Server listening on port ${PORT}`)); 